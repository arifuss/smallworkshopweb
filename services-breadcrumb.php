<?php 
$currentUrl = $_SERVER['REQUEST_URI'];
$currentUrlReplaced = str_replace('.html', '.php', $currentUrl);
$sqlCatSelected = "SELECT * FROM category WHERE external_link = '{$currentUrlReplaced}'";
$resultCatSelected = $conn->query($sqlCatSelected);
$rowCatSelected = $resultCatSelected->fetch_assoc();

$sqlCat = "
SELECT * FROM category WHERE section_id = 3 AND show_in_nav = 1 AND published = 1
ORDER BY sort_order ASC
";
$resultCat = $conn->query($sqlCat);
?>

<div class="row gray full-width page-header vertical-align-table">
	<div class="row full-width padding-top-bottom-50 vertical-align-cell">
		<div class="row">
			<div class="page-header-left">
				<h1><?php echo strtoupper($rowCatSelected['title']) ?></h1>
			</div>
			<div class="page-header-right">
				<div class="bread-crumb-container">
					<label>You Are Here:</label>
					<ul class="bread-crumb">
						<li>
							<a title="Our Services" href="services.php">
								OUR SERVICES
							</a>
						</li>
						<li class="separator">
							&#47;
						</li>
						<li>
							<?php echo strtoupper($rowCatSelected['title']) ?>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
