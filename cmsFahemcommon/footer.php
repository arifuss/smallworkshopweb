<?php 
$sqlSer = "SELECT * FROM category WHERE section_id = 3 AND published = 1 AND show_in_nav = 1 ORDER BY sort_order ASC";
$resultSer = $conn->query($sqlSer);
?>
<div class="row yellow full-width padding-top-bottom-30">
	<div class="row">
		<div class="column column-1-3">
			<ul class="contact-details-list">
				<li class="sl-small-phone">
					<p>Phone:<br>
					+65 9613 5091</p>
				</li>
			</ul>
		</div>
		<div class="column column-1-3">
			<ul class="contact-details-list">
				<li class="sl-small-location">
					<p>1 Kaki Bukit Road 1<br>
					#04-26 Enterprise One, S 415934</p>
				</li>
			</ul>
		</div>
		<div class="column column-1-3">
			<ul class="contact-details-list">
				<li class="sl-small-mail">
					<p>E-mail:<br>
					<a href="mailto:smallworkshop2023@gmail.com">smallworkshop2023@gmail.com</a></p>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="row gray full-width page-padding-top padding-bottom-50">
	<div class="row row-4-4">
		<div class="column column-1-3">
			<h6 class="box-header">About Us</h6>
			<p class="description t1">Founded by Hossan, the small workshop has estabilished itself as one of the greatest and prestigious providers of construction focused interior renovation services and building.</p>
			<ul class="social-icons yellow margin-top-26">
				<li>
					<a target="_blank" href="https://facebook.com/profile.php?id=61551068769667" class="social-facebook" title="facebook"></a>
				</li>
				<!--<li>
					<a target="_blank" href="https://twitter.com/" class="social-twitter" title="twitter"></a>
				</li>-->
			</ul>
		</div>
		<div class="column column-1-3">
			<h6 class="box-header">Our Services</h6>
			<ul class="list margin-top-20">
				<?php while($rowSer = $resultSer->fetch_assoc()) { ?>
				<li class="template-bullet"><?php echo $rowSer['title'] ?></li>
				<?php } ?>
			</ul>
		</div>
		<div class="column column-1-3">
			<h6 class="box-header">Location</h6>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.738472925414!2d103.89947647405228!3d1.3330672616314703!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da17f0878542a9%3A0x820616f6027bb90c!2sEnterprise%20One!5e0!3m2!1sen!2ssg!4v1694067627723!5m2!1sen!2ssg" style="border:0;margin-top:15px;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
		</div>
	</div>
</div>
<div class="row align-center padding-top-bottom-30">
	<span class="copyright">© Copyright 2023 Design & Developed by <a href="https://www.cubosale.com/" title="CuboSale" target="_blank">CuboSale</a></span>
</div>