<?php ?>
<title>Small Workshop</title>
<!--meta-->
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.2" />
<meta name="format-detection" content="telephone=no" />
<meta name="keywords" content="Construction, Renovation" />
<meta name="description" content="Responsive Construction Renovation Template" />
<!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">-->
<!--slider revolution-->
<link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />
<!--style-->
<link href='//fonts.googleapis.com/css?family=Raleway:100,300,400,500,600,700,900' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="style/reset.css">
<link rel="stylesheet" type="text/css" href="style/superfish.css">
<link rel="stylesheet" type="text/css" href="style/prettyPhoto.css">
<link rel="stylesheet" type="text/css" href="style/jquery.qtip.css">
<link rel="stylesheet" type="text/css" href="style/style.css">
<link rel="stylesheet" type="text/css" href="style/animations.css">
<link rel="stylesheet" type="text/css" href="style/responsive.css">
<link rel="stylesheet" type="text/css" href="style/odometer-theme-default.css">
<!--fonts-->
<link rel="stylesheet" type="text/css" href="fonts/streamline-small/styles.css">
<link rel="stylesheet" type="text/css" href="fonts/streamline-large/styles.css">
<link rel="stylesheet" type="text/css" href="fonts/template/styles.css">
<link rel="stylesheet" type="text/css" href="fonts/social/styles.css">
<link rel="shortcut icon" href="images/favicon.ico">
