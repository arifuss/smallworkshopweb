<?php 
$sqlSec = "SELECT * FROM section WHERE published = 1 AND button_position = 'Top'";
$resultSec = $conn->query($sqlSec);

$currentUrl = $_SERVER['REQUEST_URI'];
$currentUrlReplaced = str_replace('.html', '.php', $currentUrl);
$sqlSecSelected = "SELECT section_id, external_link FROM section WHERE external_link = '{$currentUrlReplaced}'";
$resultSecSelected = $conn->query($sqlSecSelected);
$rowSecSelected = $resultSecSelected->fetch_assoc();
$count = 1;

$sqlCatSelected = "
SELECT s.section_id FROM section s
LEFT JOIN (category c) ON (s.section_id = c.section_id)
WHERE c.external_link = '{$currentUrlReplaced}'";
$resultCatSelected = $conn->query($sqlCatSelected);
$rowCatSelected = $resultCatSelected->fetch_assoc();
?>
<nav>
	<ul class="sf-menu">
		<?php while($rowSec = $resultSec->fetch_assoc()) {
			$urlReplacedSecSelected = str_replace('.php', '.html', $rowSecSelected['external_link']);
			$urlReplacedRowSec = str_replace('.php', '.html', $rowSec['external_link']);

			/*
			echo $urlReplacedSecSelected . ' => url 1 <br/>';
			echo $urlReplacedRowSec . ' => url 2 <br/>';
			*/

			/*
			echo $rowSecSelected['external_link'] . ' => url 1 <br/>';
			echo $rowSec['external_link'] . ' => url 2 <br/>';
			echo $rowCatSelected['section_id'] . ' => url 1a <br/>';
			echo $rowSec['section_id'] . ' => url 2a <br/>';
			*/

			if ($urlReplacedSecSelected == $urlReplacedRowSec || $rowCatSelected['section_id'] == $rowSec['section_id']) {
		?>
		<li class="selected">
		<?php } else if ($currentUrl == '/' && $count == 1) { ?>
		<li class="selected">
		<?php } else { ?>
		<li>
		<?php } ?>
		<a href="<?php echo $rowSec['external_link'] ?>" title="<?php echo $rowSec['title'] ?>">
			<?php echo strtoupper($rowSec['title']); ?>
		</a>
		<?php $count++;
		$sqlCat = "
		SELECT * FROM category WHERE section_id = {$rowSec['section_id']} AND show_in_nav = 1 AND published = 1
		ORDER BY sort_order ASC
		";
		$resultCat = $conn->query($sqlCat);
		if ($resultCat->num_rows > 0) { ?>
		<ul>	
		<?php while($rowCat = $resultCat->fetch_assoc()) { 
		?>
			<li>
				<a href="<?php echo $rowCat['external_link'] ?>" title="<?php echo $rowCat['title'] ?>">
					<?php echo $rowCat['title'] ?>
				</a>
			</li>
		<?php } ?>
		</ul>
		<?php } ?>
		</li>
		<?php } ?>
	</ul>
</nav>
