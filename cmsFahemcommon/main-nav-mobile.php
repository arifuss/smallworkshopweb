<?php 
$sqlSec = "SELECT * FROM section WHERE published = 1 AND button_position = 'Top'";
$resultSec = $conn->query($sqlSec);
?>
<nav>
	<ul class="mobile-menu collapsible-mobile-submenus">
		<?php while($rowSec = $resultSec->fetch_assoc()) { 
			if ($rowSec['title'] == 'Home') {
		?>
		<li class="selected">
		<?php } else { ?>
		<li>
		<?php } ?>
		<a href="<?php echo $rowSec['external_link'] ?>" title="<?php echo $rowSec['title'] ?>">
			<?php echo strtoupper($rowSec['title']); ?>
		</a>
		<?php
		$sqlCat = "
		SELECT * FROM category WHERE section_id = {$rowSec['section_id']} AND show_in_nav = 1 AND published = 1
		ORDER BY sort_order ASC
		";
		$resultCat = $conn->query($sqlCat);
		if ($resultCat->num_rows > 0) { ?>
		<a href="#" class="template-arrow-menu"></a>
		<ul>	
		<?php while($rowCat = $resultCat->fetch_assoc()) { 
		?>
			<li>
				<a href="service_hacking.html" title="Hacking">
					<?php echo $rowCat['title'] ?>
				</a>
			</li>
		<?php } ?>
		</ul>
		<?php } ?>
		</li>
		<?php } ?>
	</ul>
</nav>
