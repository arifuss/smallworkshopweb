<?php ?>
<div class="header-top-bar-container clearfix">
	<div class="header-top-bar">
		<ul class="contact-details clearfix">
			<li class="template-phone">
				<a href="tel:+6596135091">+65 9613 5091</a>
			</li>
			<li class="template-mail">
				<a href="mailto:smallworkshop2023@gmail.com">smallworkshop2023@gmail.com</a>
			</li>
			<li class="template-clock">
				Mon - Sat: 09.00 - 18.00
			</li>
		</ul>
		<!--<div class="search-container">
			<a class="template-search" href="#" title="Search"></a>
			<form class="search" action="search.html">
				<input type="text" name="s" placeholder="Search..." value="Search..." class="search-input hint">
				<fieldset class="search-submit-container">
					<span class="template-search"></span>
					<input type="submit" class="search-submit" value="">
				</fieldset>
			</form>
		</div>-->
		<ul class="social-icons">
			<li>
				<a target="_blank" href="https://facebook.com/profile.php?id=61551068769667" class="social-facebook" title="facebook"></a>
			</li>
			<!--<li>
				<a target="_blank" href="https://twitter.com/" class="social-twitter" title="twitter"></a>
			</li>
			<li>
				<a href="https://pinterest.com/quanticalabs/" class="social-pinterest" title="pinterest"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-google-plus"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-youtube"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-linkedin"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-pinterest"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-dribble"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-skype"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-tumblr"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-instagram"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-xing"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-foursquare"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-rss"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-behance"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-picasa"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-stumbleupon"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-vimeo"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-houzz"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-yelp"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-github"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-reddit"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-soundcloud"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-vk"></a>
			</li>
			<li>
				<a title="" href="https://1.envato.market/quanticalabs" class="social-vine"></a>
			</li>
			-->
		</ul>
	</div>
	<a href="#" class="header-toggle template-arrow-up"></a>
</div>
