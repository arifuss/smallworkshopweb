<!DOCTYPE html>
<html>
	<head>
		<?php include('cmsFahemcommon/head-tag.php'); ?>
	</head>
	<body>

		<?php 
		error_reporting(E_ERROR | E_PARSE);
		include('cmsFahemcommon/database.php');
		?>

		<div class="site-container">
			<?php include('cmsFahemcommon/top-header.php'); ?>
			<?php include('cmsFahemcommon/header.php'); ?>

			<div class="theme-page">

				<?php include('services-breadcrumb.php'); ?>

				<div class="clearfix">
					<div class="row margin-top-70">
						<?php include('services-leftNav.php'); ?>

						<div class="column column-3-4">
							<div class="row">
								<div class="column column-1-2">
									<a href="images/samples/750x500/image_03.jpg" class="prettyPhoto re-preload" title="<?php echo $rowCat['title'] ?>">
										<img src='images/samples/480x320/image_03.jpg' alt='img'>
									</a>
								</div>
								<div class="column column-1-2">
									<a href="images/samples/750x500/image_06.jpg" class="prettyPhoto re-preload" title="<?php echo $rowCat['title'] ?>">
										<img src='images/samples/480x320/image_08.jpg' alt='img'>
									</a>
								</div>
							</div>
							<div class="row page-margin-top">
								<div class="column-1-1">
									<h3 class="box-header">SERVICE OVERVIEW</h3>
									<p class="description t1">Painting in construction is a vital step that adds the finishing touch to any project. It involves the application of paint or coatings to surfaces, walls, ceilings, and other structures. Painting serves both functional and aesthetic purposes. Functionally, it protects surfaces from moisture, weathering, and corrosion, extending their lifespan. Aesthetically, it enhances the overall appearance of the space, creating a visually appealing environment.</p>
									<p class="description t1">Skilled painters use various techniques and tools to achieve a smooth and even finish, ensuring quality and durability. Whether it's a commercial building or a residential space, painting in construction plays a significant role in transforming a structure into a stunning and inviting space.</p>
								</div>
							</div>
							<div class="row page-margin-top padding-bottom-70">
								<div class="column column-1-1">
									<h4 class="box-header">WHY CHOOSE US</h4>
									<p class="description t1 margin-top-34">With over years of experience and a real focus on customer satisfaction, you can rely on us for your next project.
									We provide a professional renovation and installation services with a real focus on customer satisfaction.</p>
									<ul class="list margin-top-20">
										<li class="template-bullet">Financial Responsibility to Our Clients</li>
										<li class="template-bullet">Superior Quality and Craftsmanship</li>
										<li class="template-bullet">Quality and Value to the Projects We Deliver</li>
										<li class="template-bullet">Highest Standards in Cost Control</li>
										<li class="template-bullet">On Time and on Budget</li>
										<li class="template-bullet">Real Focus on Customer Satisfaction</li>
									</ul>
								</div>
								<!--<div class="column column-1-2">
									<h4 class="box-header">POPULAR QUESTIONS</h4>
									<ul class="accordion margin-top-40 clearfix">
										<li>
											<div id="accordion-renovation-cost">
												<h5>Why does a renovation project cost so much?</h5>
											</div>
											<p class="description t1">Morbi nulla tortor, degnissim at node cursus euismod est arcu.
											Nomad turbina uter vehicula justo magna paetos in accumsan
											tempus, terminal ullamcorper a quam suscipit.</p>
										</li>
										<li>
											<div id="accordion-project-timeline">
												<h5>What is the timeline for the project?</h5>
											</div>
											<p class="description t1">Morbi nulla tortor, degnissim at node cursus euismod est arcu.
											Nomad turbina uter vehicula justo magna paetos in accumsan
											tempus, terminal ullamcorper a quam suscipit.</p>
										</li>
										<li>
											<div id="accordion-construction-budget">
												<h5>What is the total budget for construction?</h5>
											</div>
											<p class="description t1">Morbi nulla tortor, degnissim at node cursus euismod est arcu.
											Nomad turbina uter vehicula justo magna paetos in accumsan
											tempus, terminal ullamcorper a quam suscipit.</p>
										</li>
										<li>
											<div id="accordion-project-initiation">
												<h5>How is renovation project initiated?</h5>
											</div>
											<p class="description t1">Morbi nulla tortor, degnissim at node cursus euismod est arcu.
											Nomad turbina uter vehicula justo magna paetos in accumsan
											tempus, terminal ullamcorper a quam suscipit.</p>
										</li>
									</ul>
								</div>-->
							</div>
						</div>
					</div>
				</div>
			</div>

			<?php include('cmsFahemcommon/footer.php'); ?>

		</div>
		<a href="#top" class="scroll-top animated-element template-arrow-up" title="Scroll to top"></a>
		<!--js-->
		<script type="text/javascript" src="js/jquery-3.6.0.min.js"></script>
		<!--slider revolution-->
		<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<script type="text/javascript" src="js/jquery.ba-bbq.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.12.1.custom.min.js"></script>
		<script type="text/javascript" src="js/jquery.ui.touch-punch.min.js"></script>
		<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
		<script type="text/javascript" src="js/jquery.easing.1.4.1.min.js"></script>
		<script type="text/javascript" src="js/jquery.carouFredSel-6.2.1-packed.js"></script>
		<script type="text/javascript" src="js/jquery.touchSwipe.min.js"></script>
		<script type="text/javascript" src="js/jquery.transit.min.js"></script>
		<script type="text/javascript" src="js/jquery.hint.min.js"></script>
		<script type="text/javascript" src="js/jquery.costCalculator.min.js"></script>
		<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="js/jquery.qtip.min.js"></script>
		<script type="text/javascript" src="js/jquery.blockUI.min.js"></script>
		<script type="text/javascript" src="js/jquery.imagesloaded-packed.js"></script>
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/odometer.min.js"></script>
	</body>
</html>