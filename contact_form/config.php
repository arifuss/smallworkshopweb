<?php
define('_to_name', 'Hossan');
define('_to_email', 'smallworkshop2023@gmail.com');

define('_from_name', ''); //optional, if not set _to_name will be used
define('_from_email', 'smallworkshop2023@gmail.com'); //optional, if not set _to_email will be used

define('_smtp_host', 'in-v3.mailjet.com');
define('_smtp_username', 'e59107a5bdb4513881a519418052ad4d');
define('_smtp_password', 'a1a9bc37cd7b4b2d6767aeeec61663e3');
define('_smtp_port', '2525');
define('_smtp_secure', 'tls'); //ssl or tls

define('_subject_email', 'Small Workshop: Contact from Website');

define('_def_name', 'Your Name *');
define('_def_email', 'Your Email *');
define('_def_phone', 'Your Phone');
define('_def_message', 'Message *');

define('_msg_invalid_data_name', 'Please enter your name.');
define('_msg_invalid_data_email', 'Please enter valid e-mail.');
define('_msg_invalid_data_message', 'Please enter your message.');
define('_msg_invalid_g_recaptcha_response', 'Please check recaptcha.');

define('_msg_send_ok', 'Thank you for contacting us.');
define('_msg_send_error', 'Sorry, we can\'t send this message.');
?>
