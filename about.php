<!DOCTYPE html>
<html>
	<head>
		<?php include('cmsFahemcommon/head-tag.php'); ?>
	</head>
	<body>

		<?php 
		error_reporting(E_ERROR | E_PARSE);
		include('cmsFahemcommon/database.php');
		?>

		<div class="site-container">
			<?php include('cmsFahemcommon/top-header.php'); ?>
			<?php include('cmsFahemcommon/header.php'); ?>

			<div class="theme-page padding-bottom-66">
				<div class="row gray full-width page-header vertical-align-table">
					<div class="row full-width padding-top-bottom-50 vertical-align-cell">
						<div class="row">
							<div class="page-header-left">
								<h1>ABOUT US</h1>
							</div>
							<div class="page-header-right">
								<div class="bread-crumb-container">
									<label>You Are Here:</label>
									<ul class="bread-crumb">
										<li>
											<a title="HOME" href="index.php">
												HOME
											</a>
										</li>
										<li class="separator">
											&#47;
										</li>
										<li>
											ABOUT US
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix">
					<div class="row margin-top-70">
						<div class="column column-1-2 align-center re-preload">
							<div class="image-wrapper">
								<img src="images/samples/480x480/image_10.jpg" alt="" class="radius">
							</div>
						</div>
						<div class="column column-1-2">
							<h2 class="box-header align-left">WE ARE SMALL WORKSHOP</h2>
							<p class="description t1">Founded by Hossan, Small Workshop has established itself as one of the greatest and prestigious providers of construction focused interior renovation services and building. We provide a professional renovation and installation services with a real focus on customer satisfaction. Our construction Services is a multi-task company specializing in the following core areas:</p>
							<ul class="list margin-top-20">
								<li class="template-bullet">We combine Quality Workmanship, Superior Knowledge and Low Prices</li>
								<li class="template-bullet">We ensure Job is Done on Time and on Budget</li>
								<li class="template-bullet">Proven Results for Setting Exceptional Standards in Cost Control</li>
								<li class="template-bullet">Profesional Service for Private and Commercial Customers</li>
								<li class="template-bullet">Years of Experience and a Real Focus on Customer Satisfaction</li>
							</ul>
							<div class="page-margin-top">
								<a class="more" href="services.php" title="OUR SERVICES">OUR SERVICES</a>
							</div>
						</div>
					</div>
					<div class="row gray full-width page-margin-top-section padding-top-70 padding-bottom-66">
						<div class="row">
							<div class="column column-1-3">
								<ul class="features-list">
									<li class="sl-small-wrench">
										<h4>BEST VALUE</h4>
										<p>We combine quality workmanship, superior knowledge and low prices to provide you with service unmatched by our competitors.</p>
									</li>
								</ul>
							</div>
							<div class="column column-1-3">
								<ul class="features-list">
									<li class="sl-small-measure">
										<h4>SAVING TIME</h4>
										<p>We have the experience, personel and resources to make the project run smoothly. We can ensure a job is done on time.</p>
									</li>
								</ul>
							</div>
							<div class="column column-1-3">
								<ul class="features-list">
									<li class="sl-small-bucket">
										<h4>WITHIN BUDGET</h4>
										<p>Work with us involve a carefully planned series of steps, centered around a schedule we stick to and daily communication.</p>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="row full-width padding-top-70 padding-bottom-66 parallax parallax-2">	
						<div class="row">
							<h2 class="box-header">WHY CHOOSE US</h2>
						</div>
						<div class="row page-margin-top">
							<div class="column column-1-3">
								<ul class="features-list big">
									<li class="sl-large-globe">
										<div class="ornament"></div>
										<span class="number animated-element" data-value="15"></span>
										<p>Years on the International Market.</p>
									</li>
								</ul>
							</div>
							<div class="column column-1-3">
								<ul class="features-list big">
									<li class="sl-large-house-2">
										<div class="ornament"></div>
										<span class="number animated-element" data-value="135"></span>
										<p>Completed Projects and Investments.</p>
									</li>
								</ul>
							</div>
							<div class="column column-1-3">
								<ul class="features-list big">
									<li class="sl-large-briefcase">
										<div class="ornament"></div>
										<span class="number animated-element" data-value="12"></span>
										<p>Skilled Professionals.</p>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="row page-margin-top-section">
						<div class="column column-1-2">
							<h3 class="box-header">OUR VISION</h3>
							<p class="description t1">At Small Workshop, we envision transforming spaces into breathtaking havens that inspire and delight. We strive to be at the forefront of innovative design, using our expertise to breathe new life into homes, offices, and commercial spaces. With our unwavering commitment to quality craftsmanship and attention to detail, we aim to exceed expectations, creating unique and functional environments that reflect the individuality of our clients. We believe in collaborating closely with our clients, understanding their vision, and translating it into reality. Our ultimate goal is to create spaces that not only enhance aesthetics but also improve the quality of life for those who occupy them.</p>
						</div>
						<div class="column column-1-2">
							<h3 class="box-header">OUR MISSION</h3>
							<p class="description t1">Our mission at The Small Workshop is to revolutionize the way spaces are transformed, providing exceptional service and delivering exceptional results. We are dedicated to providing our clients with a seamless and stress-free renovation experience, from start to finish. Our team of skilled professionals is committed to delivering superior craftsmanship, using the finest materials and innovative techniques. We strive to exceed expectations by staying on top of the latest industry trends and technologies, ensuring that our clients receive nothing but the best. Our mission is to create beautiful, functional, and timeless spaces that enhance the lives of our clients and leave a lasting impression.</p>
						</div>
					</div>
				</div>
			</div>

			<?php include('cmsFahemcommon/footer.php'); ?>

		</div>
		<a href="#top" class="scroll-top animated-element template-arrow-up" title="Scroll to top"></a>
		<!--js-->
		<script type="text/javascript" src="js/jquery-3.6.0.min.js"></script>
		<!--slider revolution-->
		<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<script type="text/javascript" src="js/jquery.ba-bbq.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.12.1.custom.min.js"></script>
		<script type="text/javascript" src="js/jquery.ui.touch-punch.min.js"></script>
		<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
		<script type="text/javascript" src="js/jquery.easing.1.4.1.min.js"></script>
		<script type="text/javascript" src="js/jquery.carouFredSel-6.2.1-packed.js"></script>
		<script type="text/javascript" src="js/jquery.touchSwipe.min.js"></script>
		<script type="text/javascript" src="js/jquery.transit.min.js"></script>
		<script type="text/javascript" src="js/jquery.hint.min.js"></script>
		<script type="text/javascript" src="js/jquery.costCalculator.min.js"></script>
		<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="js/jquery.qtip.min.js"></script>
		<script type="text/javascript" src="js/jquery.blockUI.min.js"></script>
		<script type="text/javascript" src="js/jquery.imagesloaded-packed.js"></script>
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/odometer.min.js"></script>
	</body>
</html>