<!DOCTYPE html>
<html>
	<head>
		<?php include('cmsFahemcommon/head-tag.php'); ?>
	</head>
	<body>

		<?php 
		error_reporting(E_ERROR | E_PARSE);
		include('cmsFahemcommon/database.php');
		?>

		<div class="site-container">
			<?php include('cmsFahemcommon/top-header.php'); ?>
			<?php include('cmsFahemcommon/header.php'); ?>

			<div class="theme-page padding-bottom-70">
				<div class="row gray full-width page-header vertical-align-table">
					<div class="row full-width padding-top-bottom-50 vertical-align-cell">
						<div class="row">
							<div class="page-header-left">
								<h1>OUR SERVICES</h1>
							</div>
							<div class="page-header-right">
								<div class="bread-crumb-container">
									<label>You Are Here:</label>
									<ul class="bread-crumb">
										<li>
											<a title="Home" href="index.php">
												HOME
											</a>
										</li>
										<li class="separator">
											&#47;
										</li>
										<li>
											OUR SERVICES
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix">
					<div class="row">
						<ul class="services-list clearfix padding-top-70">
							<li>
								<a href="service_hacking.php" title="Hacking">
									<img src="images/samples/390x260/image_01.jpg" alt="">
								</a>
								<h4 class="box-header"><a href="service_hacking.php" title="Hacking">HACKING</a></h4>
								<p>We can help you bring new life to existing rooms and develop unused spaces.</p>
							</li>
							<li>
								<a href="service_cleaning.php" title="Cleaning">
									<img src="images/samples/390x260/image_02.jpg" alt="">
								</a>
								<h4 class="box-header"><a href="service_cleaning.php" title="Cleaning">CLEANING</a></h4>
								<p>From initial design and project specification to archieving a high end finish.</p>
							</li>
							<li>
								<a href="service_painting.php" title="Painting">
									<img src="images/samples/390x260/image_03.jpg" alt="">
								</a>
								<h4 class="box-header"><a href="service_painting.php" title="Painting">PAINTING</a></h4>
								<p>We offer quality painting solutions for interior and exterior.</p>
							</li>
							<li>
								<a href="service_reinstatement.php" title="Reinstatement">
									<img src="images/samples/390x260/image_04.jpg" alt="">
								</a>
								<h4 class="box-header"><a href="service_reinstatement.php" title="Reinstatement">REINSTATEMENT</a></h4>
								<p>Get back your dream work without any damage with skilled workmanship.</p>
							</li>
							<li>
								<a href="service_tiles.php" title="Tiles">
									<img src="images/samples/390x260/image_05.jpg" alt="">
								</a>
								<h4 class="box-header"><a href="service_tiles.php" title="Tiles">TILES</a></h4>
								<p>Renovate or replace with your dream tiles model in quick time.</p>
							</li>
						</ul>
					</div>
				</div>
			</div>

			<?php include('cmsFahemcommon/footer.php'); ?>

		</div>
		<a href="#top" class="scroll-top animated-element template-arrow-up" title="Scroll to top"></a>
		<!--js-->
		<script type="text/javascript" src="js/jquery-3.6.0.min.js"></script>
		<!--slider revolution-->
		<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<script type="text/javascript" src="js/jquery.ba-bbq.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.12.1.custom.min.js"></script>
		<script type="text/javascript" src="js/jquery.ui.touch-punch.min.js"></script>
		<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
		<script type="text/javascript" src="js/jquery.easing.1.4.1.min.js"></script>
		<script type="text/javascript" src="js/jquery.carouFredSel-6.2.1-packed.js"></script>
		<script type="text/javascript" src="js/jquery.touchSwipe.min.js"></script>
		<script type="text/javascript" src="js/jquery.transit.min.js"></script>
		<script type="text/javascript" src="js/jquery.hint.min.js"></script>
		<script type="text/javascript" src="js/jquery.costCalculator.min.js"></script>
		<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="js/jquery.qtip.min.js"></script>
		<script type="text/javascript" src="js/jquery.blockUI.min.js"></script>
		<script type="text/javascript" src="js/jquery.imagesloaded-packed.js"></script>
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/odometer.min.js"></script>
	</body>
</html>