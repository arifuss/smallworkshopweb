<!DOCTYPE html>
<html>
	<head>
		<?php include('cmsFahemcommon/head-tag.php'); ?>
	</head>
	<body>

		<?php 
		error_reporting(E_ERROR | E_PARSE);
		include('cmsFahemcommon/database.php');

		$sqlSiteKey = "
		SELECT value FROM setting WHERE key_text = 'cp.recaptchaSiteKey'";
		$resultSiteKey = $conn->query($sqlSiteKey);
		$rowSiteKey = $resultSiteKey->fetch_assoc();
		?>

		<div class="site-container">
			<?php include('cmsFahemcommon/top-header.php'); ?>
			<?php include('cmsFahemcommon/header.php'); ?>

			<div class="theme-page padding-bottom-66">
				<div class="row gray full-width page-header vertical-align-table">
					<div class="row full-width padding-top-bottom-50 vertical-align-cell">
						<div class="row">
							<div class="page-header-left">
								<h1>CONTACT US</h1>
							</div>
							<div class="page-header-right">
								<div class="bread-crumb-container">
									<label>You Are Here:</label>
									<ul class="bread-crumb">
										<li>
											<a title="HOME" href="index.php">
												HOME
											</a>
										</li>
										<li class="separator">
											&#47;
										</li>
										<li>
											CONTACT US
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row margin-top-70">
					<div class="column column-1-3 re-smart-column">
						<div class="re-smart-column-wrapper">
							<h4 class="box-header">CONTACT DETAILS</h4>
							<ul class="features-list page-margin-top clearfix">
								<li class="sl-small-location">
									<p>1 Kaki Bukit Road 1<br>#04-26 Enterprise One,<br>Singapore 415934</p>
								</li>
								<li class="sl-small-phone">
									<p>Phone:<br>+65 9613 5091</p>
								</li>
								<li class="sl-small-mail">
									<p>E-mail:<br><a href="mailto:smallworkshop2023@gmail.com">smallworkshop2023@gmail.com</a></p>
								</li>
								<!--<li class="sl-small-fax">
									<p>Fax:<br>+65 9613 5091</p>
								</li>-->
								<li class="sl-small-clock">
									<p>Monday - Saturday: 09.00 - 18.00</p>
								</li>
							</ul>
						</div>
					</div>
					<div class="column column-2-3">
						<!--<div class="contact-map" id="map"></div>-->
						<div><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.738472925414!2d103.89947647405228!3d1.3330672616314703!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da17f0878542a9%3A0x820616f6027bb90c!2sEnterprise%20One!5e0!3m2!1sen!2ssg!4v1694196104511!5m2!1sen!2ssg" width="700" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe></div>
						<h3 class="box-header page-margin-top">DROP US A LINE</h3>
						<form class="contact-form page-margin-top" id="contact-form" method="post" action="contact_form/contact_form.php">
							<div class="row">
								<fieldset class="column column-1-2">
									<input class="text-input" name="name" type="text" value="Your Name *" placeholder="Your Name *">
									<input class="text-input" name="email" type="text" value="Your Email *" placeholder="Your Email *">
									<input class="text-input" name="phone" type="text" value="Your Phone" placeholder="Your Phone">
								</fieldset>
								<fieldset class="column column-1-2">
									<textarea name="message" placeholder="Message *">Message *</textarea>
									<div class='type-text ym-fbox-text row_captcha_code'>
							            <label for='fld_captcha_code'></label>
							            <div class='txt' id='fld_captcha_code'>
							                <span class='value'>&nbsp;</span>
							            </div>
							            <a class='mb5 reloadRobotCaptcha' onclick='Util.reloadRobotCaptcha()'><span class='glyphicon glyphicon-repeat'></span> Reload Captcha</a>
							            <script src='https://www.google.com/recaptcha/api.js'></script>
							            <div class='g-recaptcha' data-sitekey='<? echo $rowSiteKey['value'] ?>'></div>
						        	</div>
								</fieldset>
							</div>
							<div class="row margin-top-30">
								<div class="column column-1-2">
									<p class="description t1">We will contact you within one business day.</p>
								</div>
								<div class="column column-1-2 align-right">
									<input type="hidden" name="action" value="contact_form" />
									<input type="submit" name="submit" value="SEND MESSAGE" class="more active">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>

			<?php include('cmsFahemcommon/footer.php'); ?>

		</div>
		<a href="#top" class="scroll-top animated-element template-arrow-up" title="Scroll to top"></a>
		<!--js-->
		<script type="text/javascript" src="js/jquery-3.6.0.min.js"></script>
		<!--slider revolution-->
		<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<script type="text/javascript" src="js/jquery.ba-bbq.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.12.1.custom.min.js"></script>
		<script type="text/javascript" src="js/jquery.ui.touch-punch.min.js"></script>
		<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
		<script type="text/javascript" src="js/jquery.easing.1.4.1.min.js"></script>
		<script type="text/javascript" src="js/jquery.carouFredSel-6.2.1-packed.js"></script>
		<script type="text/javascript" src="js/jquery.touchSwipe.min.js"></script>
		<script type="text/javascript" src="js/jquery.transit.min.js"></script>
		<script type="text/javascript" src="js/jquery.hint.min.js"></script>
		<script type="text/javascript" src="js/jquery.costCalculator.min.js"></script>
		<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="js/jquery.qtip.min.js"></script>
		<script type="text/javascript" src="js/jquery.blockUI.min.js"></script>
		<script type="text/javascript" src="js/jquery.imagesloaded-packed.js"></script>
		<script type="text/javascript" src="//maps.google.com/maps/api/js?key=YOUR_API_KEY"></script>
		<script type="text/javascript" src="js/main.js"></script>
		<script type="text/javascript" src="js/odometer.min.js"></script>
	</body>
</html>