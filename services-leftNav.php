<?php 
$currentUrl = $_SERVER['REQUEST_URI'];
$currentUrlReplaced = str_replace('.html', '.php', $currentUrl);

$sqlCat = "
SELECT * FROM category WHERE section_id = 3 AND show_in_nav = 1 AND published = 1
ORDER BY sort_order ASC
";
$resultCat = $conn->query($sqlCat);
?>

<div class="column column-1-4">
	<ul class="vertical-menu">
		<?php while($rowCat = $resultCat->fetch_assoc()) {
			if ($currentUrlReplaced == $rowCat['external_link']) {
		?>
		<li class="selected">
		<?php } else { ?>
		<li>
		<?php } ?>
			<a href="<?php echo $rowCat['external_link'] ?>" title="<?php echo $rowCat['title'] ?>">
				<?php echo $rowCat['title'] ?>
			<span class="template-arrow-menu"></span>
			</a>
		</li>
		<?php } ?>
	</ul>
	<!--<h6 class="box-header page-margin-top">Download Brochure</h6>
	<ul class="buttons margin-top-30">
		<li class="template-arrow-circle-down">
			<a href="#" title="Download Brochure">Download</a>
		</li>
	</ul>-->
</div>